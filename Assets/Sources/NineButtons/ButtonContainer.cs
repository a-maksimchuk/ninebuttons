﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Serialization;

namespace NineButtons
{
    public class ButtonContainer : MonoBehaviour
    {
        [SerializeField] private Transform container;
        [SerializeField] private BwButton bwButtonPref;
        private readonly List<BwButton> _buttons = new List<BwButton>();
        public int ButtonNumber => _buttons.Count;
        [Range(3,9)]
        [SerializeField] private int buttonCount = 9;

        private void Start()
        {
            CreateButtons();
        }

        private void CreateButtons()
        {
            foreach (var btn in _buttons)
            {
                Destroy(btn.gameObject);
            }
            _buttons.Clear();
            while (_buttons.Count < buttonCount)
            {
                BwButton btn = Instantiate(bwButtonPref, container);
                btn.Construct(this);
                _buttons.Add(btn);
            }
        }
        
#if UNITY_EDITOR
        private void OnValidate()
        {
            if(Application.isPlaying)
                CreateButtons();
        }
#endif

        public void ChangeColorFoAllBtn()
        {
            foreach (var button in _buttons)
            {
                button.ChangeColor();
            }
        }
    }
}
