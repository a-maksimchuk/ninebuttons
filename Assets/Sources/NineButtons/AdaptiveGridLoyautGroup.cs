﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdaptiveGridLoyautGroup : GridLayoutGroup
{
    [SerializeField] private bool adapt = true;
    
#if UNITY_EDITOR
    protected override void OnValidate()
    {
        CalculateChildSize();
        base.OnValidate();
    }

#endif

    private void CalculateChildSize()
    {
        var isLandscape = Screen.width > Screen.height;
        float size = 0;
        if (isLandscape)
        {
            size = (rectTransform.rect.height-spacing.y*constraintCount) / constraintCount;
        }
        else
        {
            size = (rectTransform.rect.width-spacing.x*constraintCount) / constraintCount;
        }
        
        cellSize = new Vector2(size, size);
    }

    public override void CalculateLayoutInputHorizontal()
    {
        CalculateChildSize();
        base.CalculateLayoutInputHorizontal();
    }

    public override void CalculateLayoutInputVertical()
    {
        CalculateChildSize();
        base.CalculateLayoutInputVertical();
    }
}
