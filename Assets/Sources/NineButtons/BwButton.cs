﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NineButtons
{
    public class BwButton : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Image image;
        [SerializeField] private TextMeshProUGUI textView;
        private ButtonContainer _buttonContainer;
        private bool _isWhite = true;
        private int clickCounter = 0;
        
        public void Construct(ButtonContainer buttonContainer)
        {
            _buttonContainer = buttonContainer;
            textView.text = (_buttonContainer.ButtonNumber + 1).ToString();
        }

        private void Start()
        {
            button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            clickCounter++;
            if(clickCounter%_buttonContainer.ButtonNumber==0)
                _buttonContainer.ChangeColorFoAllBtn();
            else
                ChangeColor();
        }

        public void ChangeColor()
        {
            _isWhite = !_isWhite;
            image.color = _isWhite ? Color.white : Color.black;
            textView.color = _isWhite ? Color.black : Color.white;
        }
    }
}
